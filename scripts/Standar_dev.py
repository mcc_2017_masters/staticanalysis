import os.path
import StringIO
import csv
import math
from random import randint



def create_list(n):
    """ create_list
      Creates a list of one thousand integers on List_num.csv.
      If List_num.csv exists, the file is overwritten.
    """
    #change directory
    dir = r"/home/devuser/PycharmProjects/Analisis"
    if not os.path.exists(dir):
        os.mkdir(dir)
    my_list = []

    while len(my_list) < n:
        num = randint(0, 100)
        my_list.append([num])

    with open(os.path.join(dir, "List_num" + '.csv'), "w") as f:

        csvfile = StringIO.StringIO()
        csvwriter = csv.writer(csvfile)

        for l in my_list:
            csvwriter.writerow(l)
        for a in csvfile.getvalue():
            f.writelines(a)

def get_data():
    """ get_data

    Returns a list of integers from Problem9.csv

    """
    file_data = open('List_num.csv', "r")
    file_data_list = []
    for l in file_data:
        file_data_list.append(int(l))

    return file_data_list

def get_mean():
    """ get_mean
      Returns the mean of a list of numbers obtained from Problem9.csv
    """
    file_data_list = get_data()

    if len(file_data_list) == 0:
        return 0

    additions = 0
    for num in file_data_list:
        additions += num

    return additions/len(file_data_list)

def get_standard_deviation(n):
    """ get_standard_deviation
      Returns the standard deviation of a list of numbers obtained from Problem9.csv
    """
    create_list(n)
    file_data_list = get_data()
    mean = get_mean()
    stdAdditions = 0
    for num in file_data_list:
        stdAdditions += pow(num-mean, 2)
    var = stdAdditions/(len(file_data_list)-1)

    return math.sqrt(var)


def main():

    print "Standard deviation: "
    print get_standard_deviation(1000)




# execute main
if __name__ == "__main__":
    main()